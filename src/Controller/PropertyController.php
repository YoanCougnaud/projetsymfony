<?php

namespace App\Controller;

use App\Entity\Property;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;


class PropertyController extends AbstractController{
    // /**
    //  * @var Environment
    //  */
    // private $twig;
    
    // public function __construct(Environment $twig)
    // {
    //     $this->twig = $twig;
    // }

    /**
     * @var PropertyRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;
    
    public function __construct(PropertyRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }
    
    
    
    /**
     * @Route("/maison", name="maison")
     * @return Response
     */
    public function index(): Response {

        // $property = new Property();

        // $property->setTitle('Mon premier bien')
        // ->setSurface(90)
        // ->setFloor(2)
        // ->setDescription('c\'est une description')
        // ->setRooms(3)
        // ->setBedrooms(2)
        // ->setPrice(150000)
        // ->setCity('Béziers')
        // ->setPostalCode('34500')
        // ->setAddress('mon adresse')
        // ->setHeat(1);

        // $entityproperty = $this->getDoctrine()->getManager();
        // $entityproperty->persist($property);
        // $entityp->roperty->flush();

        // $property = $this->repository->findAllVisible();
        // $property[0]->setSold(true);
        // $this->em->flush();

        // $repository = $this->getDoctrine()->getRepository(Property::class);
        // dump($repository);

        return $this->render('property/index.html.twig', ['current_menu' => 'properties']);
    }
    /**
     * @Route("/maison/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*"})
     * @return Response
     */
    public function show(Property $property, string $slug): Response
    {

        if($property->getSlug() !== $slug){
            return $this->redirectToRoute( 'property.show',[
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ], 301);
        }
        // $property = $this->repository->find($id);
        return $this->render('property/show.html.twig', [
            'property' => $property, 
            'current_menu' => 'properties']);
    }
}
