<?php

namespace App\Form;

use App\Entity\Property;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null,['label'=> 'Titre'])
            ->add('description')
            ->add('surface')
            ->add('rooms', null,['label'=> 'pièces'])
            ->add('bedrooms', null,['label'=> 'Chambres'])
            ->add('floor',null,['label'=> 'Etage'])
            ->add('price', null,['label'=> 'Prix'])
            ->add('heat', ChoiceType::class, ['choices' => $this->getChoices(), 'label' => 'Chauffage'])
            ->add('city', null,['label'=> 'Ville'])
            ->add('address', null,['label'=> 'Adresse'])
            ->add('postal_code', null,['label'=> 'Code postal'])
            ->add('sold', null,['label'=> 'Vendu'])
            ->add('created_at', null,['label'=> 'Crée le'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,'translation_domain' => 'forms'
        ]);
    }

    public function getChoices()
    {
        $choices = Property::HEAT;
        $output = [];
        foreach($choices as $k => $v){
            $output[$v] = $k;
        }
        return $output;
    }
}
